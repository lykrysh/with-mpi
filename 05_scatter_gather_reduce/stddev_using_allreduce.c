// stddev_using_allreduce.c
#include "average.h"
#include <stdio.h>
#include <mpi.h>
#include <math.h>

int main(int argc, char **argv)
{
	if(argc != 2) {
		fprintf(stderr, "Usage: stddev num_elements_per_proc\n");
		exit(1);
	}
	int numprocele = atoi(argv[1]);

	MPI_Init(NULL, NULL);

	int rank, world;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world);

	// on ALL procs, create array of elements
	srand(time(NULL) * rank); // so unique upon rank
	float *randnums = NULL;
	randnums = create_rand_nums(numprocele);

	float localsum = 0;
	int i;
	for(i = 0; i < numprocele; i++)
		localsum += randnums[i];

	// compute (global) mean
	float globalsum;
	MPI_Allreduce(&localsum, &globalsum, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
	float mean = globalsum / numprocele / world;

	// compute local sum of the squared differences from the mean
	float localsqdiff = 0;
	for(i = 0; i < numprocele; i++)
		localsqdiff += (randnums[i] - mean) * (randnums[i] - mean);
	printf("local sq diff sum at proc %d: %f\n", rank, localsqdiff);

	float globalsqdiff = 0;
	MPI_Reduce(&localsqdiff, &globalsqdiff, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

	if(rank == 0) {
		float stddev = sqrt(globalsqdiff / numprocele / world);
		printf("GLOBAL sq diff: %f ", stddev);
		printf("(mean: %f)\n", mean);
	}

	free(randnums);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;
}
