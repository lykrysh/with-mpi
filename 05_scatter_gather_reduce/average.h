#ifndef AVERAGE_H
#define AVERAGE_H

#include <stdlib.h>
#include <time.h>
#include <assert.h>

float *create_rand_nums(int n)
{
	float *temp = (float *)malloc(sizeof(float) * n);
	assert(temp != NULL);
	int i;
	for(i = 0; i < n; i++)
		temp[i] = (rand() / (float)RAND_MAX);
	return temp;
}

float computeavg(float *arr, int n)
{
	float sum = 0.f;
	int i;
	for(i = 0; i < n; i++)
		sum += arr[i];
	return sum / n;
}

#endif // ifndef AVERAGE_H
