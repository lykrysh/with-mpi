// average_3_using_reduce.c
#include "average.h"
#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
	if(argc != 2) {
		fprintf(stderr, "Usage: average num_elements_per_proc\n");
		exit(1);
	}
	int numprocele = atoi(argv[1]);

	MPI_Init(NULL, NULL);

	int rank, world;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world);

	// on ALL procs, create array of elements
	srand(time(NULL) * rank); // so unique upon rank
	float *rand_nums = NULL;
	rand_nums = create_rand_nums(numprocele);

	float localsum = 0;
	int i;
	for(i = 0; i < numprocele; i++)
		localsum += rand_nums[i];
	printf("Proc %d's local sum is %f; local average is %f\n", rank, localsum, localsum/numprocele);

	float globalsum;
	MPI_Reduce(&localsum, &globalsum, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

	if(rank == 0) {
		printf("GLOBAL SUM is %f; GLOBAL AVERAGE is %f\n", globalsum, globalsum/numprocele/world);
	}

	free(rand_nums);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;
}
