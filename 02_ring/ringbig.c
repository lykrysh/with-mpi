#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

#define MAXNUM 100
#define TAG 0

int getrand(int rank)
{
	srand(time(NULL) * rank);
	return (rand()/(float)RAND_MAX) * MAXNUM;
}

int main(int argc, char **argv) {
	MPI_Init(NULL, NULL);

	int worldsize, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &worldsize);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int prevrank = (rank == 0) ? (worldsize - 1) : (rank - 1);
	int nextrank = (rank + 1) % worldsize;
	int tokenarr[MAXNUM];
	int randomamount;

	if(worldsize < 2)
		MPI_Abort(MPI_COMM_WORLD, 1);

	randomamount = getrand(rank);
	MPI_Send(tokenarr, randomamount, MPI_INT, nextrank, TAG, MPI_COMM_WORLD);
	printf("SENT [%d] from %d to %d\n", randomamount, rank, nextrank);

	MPI_Status stat;
	MPI_Probe(prevrank, TAG, MPI_COMM_WORLD, &stat);
	MPI_Get_count(&stat, MPI_INT, &randomamount);
	int *buf_tokenarr = (int *)malloc(sizeof(int) * randomamount);
	MPI_Recv(tokenarr, randomamount, MPI_INT, prevrank, TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	free(buf_tokenarr);
	printf("RECVed [%d] at %d\n", randomamount, rank);

	MPI_Finalize();
}

