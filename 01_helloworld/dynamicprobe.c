#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char **argv) {
	MPI_Init(NULL, NULL);

	int num_proc, rank_proc, name_len;
	char name_proc[MPI_MAX_PROCESSOR_NAME];

	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank_proc);
	MPI_Get_processor_name(name_proc, &name_len);

	if(num_proc !=  2)
		MPI_Abort(MPI_COMM_WORLD, 1);

	const int times = 10;
	int count = 0;
	int partner_rank_proc = (rank_proc + 1) % 2; // only 0 or 1

	const int max_num = 100;
	int nums[max_num];
	int amount;

	/*
	 * check status/msg-size BEFORE receiving msg
	 */

	if(rank_proc == 0) {
		srand(time(NULL));
		amount = (rand() / (float)RAND_MAX) * max_num;
		MPI_Send(nums, amount, MPI_INT, partner_rank_proc, 0, MPI_COMM_WORLD);
		printf("Proc %d sent %d numbers to %d\n", rank_proc, amount, partner_rank_proc);
	}
	else { // 1
		MPI_Status stat;
		MPI_Probe(partner_rank_proc, 0, MPI_COMM_WORLD, &stat);
		MPI_Get_count(&stat, MPI_INT, &amount); // query stats-> get msg size/amount

		// MPI_Recv(nums, max_num, MPI_INT, partner_rank_proc, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		// instead of max_num, allocate only big enough to hold the incoming msg size
		int *buf_nums = (int *)malloc(sizeof(int) * amount);
		MPI_Recv(buf_nums, amount, MPI_INT, partner_rank_proc, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		free(buf_nums);

		printf("Proc %d dynamically received %d numbers from %d: ", rank_proc, amount, partner_rank_proc);
		printf("[Msg src: %d, Tag: %d]\n", stat.MPI_SOURCE, stat.MPI_TAG);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
}


