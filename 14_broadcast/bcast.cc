#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <string>
#include <iostream>
using namespace boost;
using namespace std;

#define ROOTPROC 0

int main(int argc, char **argv)
{
	mpi::environment env{argc, argv};
	mpi::communicator world;
	string str;

	if(world.rank() == ROOTPROC) {
		str = "Hello Bunny";
	}
	
	mpi::broadcast(world, str, ROOTPROC);
	cout << "Proc " << world.rank() << " has str = " << str << endl;

	return 0;
}
