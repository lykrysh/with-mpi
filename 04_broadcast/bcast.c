#include <mpi.h>
#include <stdio.h>

int num_proc, rank;

void My_Bcast(void *data, int cnt, MPI_Datatype type, int root, MPI_Comm comm)
{
	if(rank == root) {
		int i;
		for(i=0; i < num_proc; i++) {
			if(i != rank)
				MPI_Send(data, cnt, type, i, 0, comm);
		}
	} else {
		MPI_Recv(data, cnt, type, root, 0, comm, MPI_STATUS_IGNORE);
	}
}

int main(int argc, char **argv) {
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int data;
	if(rank == 0) {
		data = 100;
		printf("Proc 0 broadcasting data %d\n", data);
		My_Bcast(&data, 1, MPI_INT, 0, MPI_COMM_WORLD);
		//MPI_Bcast(&data, 1, MPI_INT, 0, MPI_COMM_WORLD);
	} else {
		My_Bcast(&data, 1, MPI_INT, 0, MPI_COMM_WORLD);
		//MPI_Bcast(&data, 1, MPI_INT, 0, MPI_COMM_WORLD);
		printf("Proc %d received data %d from root proc\n", rank, data);
	}

	MPI_Finalize();
}

