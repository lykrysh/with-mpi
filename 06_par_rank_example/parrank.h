#ifndef PARRANK_H
#define PARRANK_H

#include <stdio.h>
#include <mpi.h>

typedef struct {
	MPI_Datatype type;
	MPI_Comm comm;
	int world;
	int rank;
	int typesize;
} mpistuff;

typedef struct {
	int comm_rank;
	union { float f; int i; } num;
} commranknum;

void parrank(void *, void *, mpistuff *);

#endif // ifndef PARRANK_H
