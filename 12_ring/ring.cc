// ring.cc
#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <string>
#include <iostream>
using namespace boost;
using namespace std;

#define TAG 0

int main(int argc, char **argv)
{
	mpi::environment env(argc, argv);
	mpi::communicator world;

	int prevrank = (world.rank() == 0) ? (world.size() - 1) : (world.rank() - 1);
	int nextrank = (world.rank() + 1) % world.size();
	string token;

	string rankstr = to_string(world.rank());
	token = "from " + rankstr;
	world.send(nextrank, TAG, token);
	cout << "SENT " << token << endl;

	world.recv(prevrank, TAG, token);
	cout << "RECV " << token << endl;

	return 0;
}
